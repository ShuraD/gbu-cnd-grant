class StateLocalStorage {
    constructor() {
        this.keys = [];
        this.values = [];
        this.levelHistory = [];
    }
    push(state) {
        this.keys = [];
        this.values = [];
        this.unpack(state);

        for (let i = 0, counter = this.keys.length; i < counter; i++) {
            if (this.isObject(this.values[i])) {
                continue;
            }
            localStorage.setItem(this.keys[i], this.values[i]);
        }
    }
    unpack(obj) {
        for (let [ key, value ] of Object.entries(obj)) {
            if (this.isObject(value)) {
                this.levelHistory.push(key);
                this.unpack(value);
            } else {
                if (this.levelHistory.length) {
                    this.keys.push(this.levelHistory.join('.') + '.' + key);
                } else {
                    this.keys.push(key);
                }
                this.values.push(value);
            }
        }
        this.levelHistory.pop();
    }
    isObject(value) {
        if (value === null) return false;
        return ((typeof value === 'function') || (typeof value === 'object'));
    }
}

module.exports = new StateLocalStorage();