import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import VueMaterial from 'vue-material'

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueMaterial)

const store = new Vuex.Store({
    state: {
        applicantInformation: { // Сведения о претенденте
            inn: null, // ИНН
            fullname: null, // Полное наименование
            legalAddress: null, // Юридический адрес
            postalAddress: null, // Почтовый адрес
            contactPerson: null, // Контактное лицо
            contactTelephone: null, // Контактный телефон
            email: null, // Электронная почта
            ceoName: null, // ФИО генерального директора
            chiefAccountantName: null, // ФИО главного бухгалтера
            isMif: null, // Является ли претендент управляющей компанией закрытого паевого инвестиционного фонда?
        },
        buildingInformation: { // Сведения о здании
            cadastralNumber: null, // Кадастровый номер
            address: null, // Адрес
            square: null, // Площадь
            registered: 'не зарегистрировано', // Право зарегистрировано на здание или помещения в нем?
            exists: 'здание', // Если право не зарегистрировано, оно существует на здание или помещение?
        },
        buildingObjectRight: { // Объект права на здание
            cadastralNumber: null, // Кадастровый номер
            type: null, // Тип здание/помещение
            square: null, // Площадь
            address: null, // Адрес
            innOwner: null, // ИНН собственника
            nameOwner: null, // Наименование собственника
            shareRight: null, // Доля в праве
            documentDetails: null, // Реквизиты правоустанавливающего документа, если сведений о собственнике нет в ЕГРН
        },
        rooms: [ // Перечень помещений в собственности

        ],
        landPlotList: [

        ],
        // ...
        requirements: { // Требования к претенденту и объекту недвижимости
            registeredAsMoscowTaxpayer: null, // Юридическое лицо и (или) его обособленное подразделение зарегистрировано в качестве налогоплательщика на территории города Москвы
            foreignLegalEntity: null, // Претендент не является иностранным юридическим лицом
            shareForeignLegalEntity: null, // Претендент не является российским юридическим лицом, в уставном (складочном) капитале которого доля участия иностранного юридического лица, местом регистрации которого является государство или территория, предоставляющая льготный налоговый режим налогообложения и (или) не предусматривающая раскрытия и предоставления информации при проведении финансовых операций в отношении такого юридического лица, в совокупности превышает 50 процентов
            procedures: null, // В отношении претендента не проводится процедура реорганизации, ликвидации или банкротства, приостановления деятельности в порядке, предусмотренном Кодексом Российской Федерации об административных правонарушениях
            administrativeResponsibility: null , // Претендент и/или арендаторы не привлекались к административной ответственности за совершение на данном объекте правонарушений, связанных с невыполнением правил, предусмотренных в связи с введением режима повышенной готовности, или нарушением требований нормативных правовых актов города Москвы, направленных на введение и обеспечение режима повышенной готовности на территории города Москвы
            unauthorizedConstruction: null, // Объект недвижимости не является самовольной постройкой
            permittedUses: null, // Виды разрешенного использования земельного участка, на котором расположен объект недвижимости, предусматривают возможность размещения такого объекта
            landRelations: null, // Земельно-правовые отношения в отношении земельного участка, на котором размещен объект недвижимости, оформлены в установленном порядке
        },
        attachedFiles: { // 6. Перечень прилагаемых документов
            constituentDocumentsFile: null, // Копии учредительных документов претендента с изменениями на день подачи заявки
            supervisorPowersFile: null, // Копия документа, подтверждающего полномочия руководителя претендента или иного лица на осуществление действий от имени претендента
            chiefAccountantFile: null, // Копия документа, подтверждающего полномочия главного бухгалтера претендента (при наличии в штате должности главного бухгалтера)
            patentTaxSystemFile: null, // Копии документов, подтверждающих применение арендаторами (субарендаторами) патентной системы налогообложения в сфере общественного питания и (или) бытового обслуживания
            calculationTaxReferenceFile: null, // Справка, содержащая расчет суммы налога на имущество и земельного налога (арендной платы за землю) за соответствующий период
            averageAnnualCostReferenceFile: null, // Справка с указанием среднегодовой стоимости принадлежащего заявителю объекта недвижимости, облагаемого налогом на имущество организаций от среднегодовой стоимости
            complianceLetterGuaranteeFile: null, // Гарантийное письмо претендента о соблюдении требований, указанных в пункте 5 заявки, на день подачи заявки
            suspensionLetterGuaranteeFile: null, // Гарантийное письмо претендента о приостановлении работы в соответствии с  указом Мэра Москвы № 12-УМ на объектах, указанных в заявке в качестве таковых
            propertyRightsFile: null, // Копии документов, подтверждающих право собственности претендента на объекты недвижимости, при отсутствии сведений о праве собственности в ЕГРН
            otherFiles: null, // Прочие документы, которые претендент считает нужным приложить
        },
        additionalComments: null, // Дополнительные комментарии претендента к заявке

    },
    mutations: {
        setApplicantInformationInn(state, payload) {
            state.applicantInformation.inn = payload.inn
        },
        setApplicantInformationFullname(state, payload) {
            state.applicantInformation.fullname = payload.fullname
        },
        setApplicantInformationLegalAddress(state, payload) {
            state.applicantInformation.legalAddress = payload.legalAddress
        },
        setApplicantInformationPostalAddress(state, payload) {
            state.applicantInformation.postalAddress = payload.postalAddress
        },
        setApplicantInformationContactPerson(state, payload) {
            state.applicantInformation.contactPerson = payload.contactPerson
        },
        setApplicantInformationContactTelephone(state, payload) {
            state.applicantInformation.contactTelephone = payload.contactTelephone
        },
        setApplicantInformationEmail(state, payload) {
            state.applicantInformation.email = payload.email
        },
        setApplicantInformationCeoName(state, payload) {
            state.applicantInformation.ceoName = payload.ceoName
        },
        setApplicantInformationChiefAccountantName(state, payload) {
            state.applicantInformation.chiefAccountantName = payload.chiefAccountantName
        },
        setApplicantInformationIsMif(state, payload) {
            state.applicantInformation.isMif = payload.isMif
        },
        // ...
        setBuildingInformationCadastralNumber(state, payload) {
            state.buildingInformation.cadastralNumber = payload.cadastralNumber
        },
        setBuildingInformationAddress(state, payload) {
            state.buildingInformation.address = payload.address
        },
        setBuildingInformationSquare(state, payload) {
            state.buildingInformation.square = payload.square
        },
        setBuildingInformationRegistered(state, payload) {
            state.buildingInformation.registered = payload.registered
        },
        setBuildingInformationExists(state, payload) {
            state.buildingInformation.exists = payload.exists
        },
        // ...
        setRequirementsRegisteredAsMoscowTaxpayer(state, payload) {
            state.requirements.registeredAsMoscowTaxpayer = payload.registeredAsMoscowTaxpayer
        },
        setRequirementsForeignLegalEntity(state, payload) {
            state.requirements.foreignLegalEntity = payload.foreignLegalEntity
        },
        setRequirementsShareForeignLegalEntity(state, payload) {
            state.requirements.shareForeignLegalEntity = payload.shareForeignLegalEntity
        },
        setRequirementsProcedures(state, payload) {
            state.requirements.procedures = payload.procedures
        },
        setRequirementsAdministrativeResponsibility(state, payload) {
            state.requirements.administrativeResponsibility = payload.administrativeResponsibility
        },
        setRequirementsUnauthorizedConstruction(state, payload) {
            state.requirements.unauthorizedConstruction = payload.unauthorizedConstruction
        },
        setRequirementsPermittedUses(state, payload) {
            state.requirements.permittedUses = payload.permittedUses
        },
        setRequirementsLandRelations(state, payload) {
            state.requirements.landRelations = payload.landRelations
        },
        // ...
        setAttachedFilesConstituentDocumentsFile(state, payload) {
            state.attachedFiles.constituentDocumentsFile = payload.constituentDocumentsFile
        },
        setAttachedFilesSupervisorPowersFile(state, payload) {
            state.attachedFiles.supervisorPowersFile = payload.supervisorPowersFile
        },
        setAttachedFilesChiefAccountantFile(state, payload) {
            state.attachedFiles.chiefAccountantFile = payload.chiefAccountantFile
        },
        setAttachedFilesPatentTaxSystemFile(state, payload) {
            state.attachedFiles.patentTaxSystemFile = payload.patentTaxSystemFile
        },
        setAttachedFilesCalculationTaxReferenceFile(state, payload) {
            state.attachedFiles.calculationTaxReferenceFile = payload.calculationTaxReferenceFile
        },
        setAttachedFilesAverageAnnualCostReferenceFile(state, payload) {
            state.attachedFiles.averageAnnualCostReferenceFile = payload.averageAnnualCostReferenceFile
        },
        setAttachedFilesComplianceLetterGuaranteeFile(state, payload) {
            state.attachedFiles.complianceLetterGuaranteeFile = payload.complianceLetterGuaranteeFile
        },
        setAttachedFilesSuspensionLetterGuaranteeFile(state, payload) {
            state.attachedFiles.suspensionLetterGuaranteeFile = payload.suspensionLetterGuaranteeFile
        },
        setAttachedFilesPropertyRightsFile(state, payload) {
            state.attachedFiles.propertyRightsFile = payload.propertyRightsFile
        },
        setAttachedFilesOtherFiles(state, payload) {
            state.attachedFiles.otherFiles = payload.otherFiles
        },
        // ...
        setAdditionalComments(state, payload) {
            state.additionalComments = payload.additionalComments
        },
    }
})

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app')
